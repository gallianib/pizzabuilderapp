package com.gallianib.pizzabuilder;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "pizza_db";
    private static final String TABLE_NAME = "orderlist";
    private static final int DATABASE_VERSION = 1;
    private SQLiteDatabase db;
    private static final String CREATE_DB_TABLE =
            "CREATE TABLE orderlist (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "name TEXT NOT NULL, " +
                    "address TEXT, " +
                    "phone TEXT, " +
                    "email TEXT, " +
                    "time DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, " +
                    "toppings TEXT NOT NULL," +
                    "total TEXT NOT NULL);";

    DatabaseHelper(Context context,String name,SQLiteDatabase.CursorFactory factory,int version) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DB_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
        onCreate(db);
    }
    public void addOrder(DataModel order) {
        db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put("name", order.getName());
            values.put("address", order.getAddress());
            values.put("phone", order.getPhone());
            values.put("toppings",order.getToppings());
            values.put("email",order.getEmail());
            values.put("total",order.getTotal());

            db.insert(TABLE_NAME, null, values);

        db.close();
    }
    public DataModel getOrder(String name){
        DataModel order = new DataModel(name);
        String selectQuery = "SELECT * FROM " + TABLE_NAME + " WHERE name = '"+name+"';";

        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            order.setId(Integer.toString(cursor.getInt(0)));
            order.setName(cursor.getString(1));
            order.setAddress(cursor.getString(2));
            order.setPhone(cursor.getString(3));
            order.setEmail(cursor.getString(4));
            order.setTime(cursor.getString(5));
            order.setToppings(cursor.getString(6));
            order.setTotal(cursor.getString(7));
            cursor.close();
        }else order = null;

        db.close();
        return order;
    }
    public int getOrderID(String name){
        int id = 0;
        DataModel order = new DataModel(name);
        String selectQuery = "SELECT id FROM " + TABLE_NAME + " WHERE name = '"+name+"';";
        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        id = (cursor.getInt(0));
        cursor.close();
        db.close();
        return id;
    }
    public ArrayList<DataModel> getAllOrders() {
        ArrayList<DataModel> orderList = new ArrayList<DataModel>();
        String selectQuery = "SELECT * FROM " + TABLE_NAME;

        db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                DataModel order = new DataModel(cursor.getString(1));
                order.setId(Integer.toString(cursor.getInt(0)));
                order.setName(cursor.getString(1));
                order.setAddress(cursor.getString(2));
                order.setPhone(cursor.getString(3));
                order.setEmail(cursor.getString(4));
                order.setTime(cursor.getString(5));
                order.setToppings(cursor.getString(6));
                order.setTotal(cursor.getString(7));
                orderList.add(order);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return orderList;
    }
    public int getOrderCount() {
        String countQuery = "SELECT id FROM " + TABLE_NAME;
        db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int i = cursor.getCount();
        cursor.close();
        db.close();
        return i;
    }

}

