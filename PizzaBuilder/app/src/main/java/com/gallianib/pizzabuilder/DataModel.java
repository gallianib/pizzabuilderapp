package com.gallianib.pizzabuilder;

public class DataModel {

    String id = "";
    String name="";
    String address="";
    String phone="";
    String toppings="";
    String time="";
    String email="";
    String total="";



    public DataModel(String name) {
        this.name = name;
    }

    public String getId(){return id;}
    public void setId(String id) {this.id = id;}
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getTime() {
        return time;
    }
    public void setTime(String time) {
        this.time = time;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getPhone() {
        return address;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getToppings() {
        return toppings;
    }
    public void setToppings(String toppings) {
        this.toppings = toppings;
    }
    public void setTotal(String total){this.total = total;}
    public String getTotal(){
        return total;
    }


}

