package com.gallianib.pizzabuilder;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;
import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView toppings;
        TextView name;
        TextView time;
        TextView total;
        TextView id;

        public ViewHolder(View itemView){
            super(itemView);
            id = (TextView)itemView.findViewById(R.id.orderId);
            toppings = (TextView)itemView.findViewById(R.id.orderToppings);
            name = (TextView)itemView.findViewById(R.id.orderName);
            time = (TextView)itemView.findViewById(R.id.orderTime);
            total = (TextView)itemView.findViewById(R.id.orderTotal);
        }
    }
    private ArrayList<DataModel> dataset;
    public CustomAdapter(ArrayList<DataModel> data){dataset=data;}
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder,final int listPosition){

        TextView orderName = holder.name;
        TextView orderTime = holder.time;
        TextView orderTotal = holder.total;
        TextView orderToppings = holder.toppings;
        TextView orderID = holder.id;

        String tp = getOrderToppings(new StringBuilder(dataset.get(listPosition).getToppings()));
        orderID.setText(dataset.get(listPosition).getId());
        orderName.setText(dataset.get(listPosition).getName());
        orderTime.setText(dataset.get(listPosition).getTime());
        orderTotal.setText(dataset.get(listPosition).getTotal());
        orderToppings.setText(tp);
    }
    public String getOrderToppings(StringBuilder toppings){
        StringBuilder top = new StringBuilder("");
        for (int r = 0;r<10;r++){
            switch(toppings.charAt(r)){
                case 'y':
                    switch (r){
                        case 0:
                            top.append(", Pepperoni");
                            break;
                        case 1:
                            top.append(", Bacon");
                            break;
                        case 2:
                            top.append(", Ham");
                            break;
                        case 3:
                            top.append(", Sausage");
                            break;
                        case 4:
                            top.append(", Jalapenos");
                            break;
                        case 5:
                            top.append(", Olives");
                            break;
                        case 6:
                            top.append(", Green Peppers");
                            break;
                        case 7:
                            top.append(", Banana Peppers");
                            break;
                        case 8:
                            top.append(", Pineapple");
                            break;
                        case 9:
                            top.append(", Onions");
                            break;
                    }
                    break;
                case 'n':
                    break;
            }
        }
        if (top.charAt(0)==','){
            top.deleteCharAt(0);
        }
        return top.toString();
    }
    @Override
    public int getItemCount(){
        return dataset.size();
    }
}
