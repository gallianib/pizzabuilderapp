package com.gallianib.pizzabuilder;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.ImageView;

public class customizePizza extends AppCompatActivity {

    private StringBuilder toppings = new StringBuilder("nnnnnnnnnnf");
    private String name;
    private String address;
    private String phone;
    private String email;
    private double total = 10;
    public String T;
    boolean del = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_pizza);

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        address = intent.getStringExtra("address");
        phone = intent.getStringExtra("phone");
        email = intent.getStringExtra("email");
    }

    public void onCheckboxClicked(View view) {

        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.pepperoni:
                if (checked) {
                    new Task().execute(0);
                    ImageView img = (ImageView) findViewById(R.id.pepperoniImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(10);
                    ImageView img = (ImageView) findViewById(R.id.pepperoniImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.bacon:
                if (checked) {
                    toppings.setCharAt(1, 'y');
                    ImageView img = (ImageView) findViewById(R.id.baconImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-1);
                    ImageView img = (ImageView) findViewById(R.id.baconImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.ham:
                if (checked) {
                    new Task().execute(2);
                    ImageView img = (ImageView) findViewById(R.id.hamImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-2);
                    ImageView img = (ImageView) findViewById(R.id.hamImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.sausage:
                if (checked) {
                    new Task().execute(3);
                    ImageView img = (ImageView) findViewById(R.id.sausageImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-3);
                    ImageView img = (ImageView) findViewById(R.id.sausageImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.jalapeno:
                if (checked) {
                    new Task().execute(4);
                    ImageView img = (ImageView) findViewById(R.id.jalapenoImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-4);
                    ImageView img = (ImageView) findViewById(R.id.jalapenoImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.olive:
                if (checked) {
                    new Task().execute(5);
                    ImageView img = (ImageView) findViewById(R.id.oliveImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-5);
                    ImageView img = (ImageView) findViewById(R.id.oliveImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.greenpepper:
                if (checked) {
                    new Task().execute(6);
                    ImageView img = (ImageView) findViewById(R.id.greenpepperImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-6);
                    ImageView img = (ImageView) findViewById(R.id.greenpepperImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.bananapepper:
                if (checked) {
                    new Task().execute(7);
                    ImageView img = (ImageView) findViewById(R.id.bananapepperImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-7);
                    ImageView img = (ImageView) findViewById(R.id.bananapepperImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.pineapple:
                if (checked) {
                    new Task().execute(8);
                    ImageView img = (ImageView) findViewById(R.id.pineappleImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-8);
                    ImageView img = (ImageView) findViewById(R.id.pineappleImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
            case R.id.onion:
                if (checked) {
                    new Task().execute(9);
                    ImageView img = (ImageView) findViewById(R.id.onionImg);
                    img.setVisibility(View.VISIBLE);
                } else {
                    new Task().execute(-9);
                    ImageView img = (ImageView) findViewById(R.id.onionImg);
                    img.setVisibility(View.INVISIBLE);
                }
                break;
        }
    }

    public void onDeliveryChecked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        if (checked) {
            toppings.setCharAt(10, 't');
            del = true;
        } else {
            toppings.setCharAt(10, 'f');
            del = false;
        }
    }

    public void onSubmitOrderClick(View view) {
        Intent i2 = new Intent(customizePizza.this, totalsPage.class);
        String toppingStr = toppings.toString();
        total = 10;
        for (int k = 0; k < 11; k++) {
            if (toppings.charAt(k) == 'y') {
                total++;
            }
        }
        if (toppings.charAt(10) == 't') {
            total += 3;
            del = true;
        }

        i2.putExtra("iname", name);
        i2.putExtra("iaddress", address);
        i2.putExtra("iphone", phone);
        i2.putExtra("iemail", email);
        i2.putExtra("itoppings", toppingStr);
        i2.putExtra("itotal", Double.toString(total));
        i2.putExtra("idel", del);
        startActivity(i2);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private class Task extends AsyncTask< Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            switch(params[0]){
                case 0:
                    toppings.setCharAt(0, 'y');
                    break;
                case 1:
                    toppings.setCharAt(1, 'y');
                    break;
                case 2:
                    toppings.setCharAt(2, 'y');
                    break;
                case 3:
                    toppings.setCharAt(3, 'y');
                    break;
                case 4:
                    toppings.setCharAt(4, 'y');
                    break;
                case 5:
                    toppings.setCharAt(5, 'y');
                    break;
                case 6:
                    toppings.setCharAt(6, 'y');
                    break;
                case 7:
                    toppings.setCharAt(7, 'y');
                    break;
                case 8:
                    toppings.setCharAt(8, 'y');
                    break;
                case 9:
                    toppings.setCharAt(9, 'y');
                    break;
                case 10:
                    toppings.setCharAt(0, 'n');
                    break;
                case -1:
                    toppings.setCharAt(1, 'n');
                    break;
                case -2:
                    toppings.setCharAt(2, 'n');
                    break;
                case -3:
                    toppings.setCharAt(3, 'n');
                    break;
                case -4:
                    toppings.setCharAt(4, 'n');
                    break;
                case -5:
                    toppings.setCharAt(5, 'n');
                    break;
                case -6:
                    toppings.setCharAt(6, 'n');
                    break;
                case -7:
                    toppings.setCharAt(7, 'n');
                    break;
                case -8:
                    toppings.setCharAt(8, 'n');
                    break;
                case -9:
                    toppings.setCharAt(9, 'n');
                    break;
            }
            return null;
        }

    }
}
