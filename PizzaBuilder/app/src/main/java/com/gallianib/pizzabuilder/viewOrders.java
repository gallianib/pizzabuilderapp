package com.gallianib.pizzabuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;


public class viewOrders extends AppCompatActivity {
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;

    EditText nameBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_orders);
        nameBox = (EditText)findViewById(R.id.searchName);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        DatabaseHelper db = new DatabaseHelper(this,null,null,1);
        data = db.getAllOrders();
        adapter = new CustomAdapter(data);
        recyclerView.setAdapter(adapter);
    }
    public void findAllOrders(View view) {
        DatabaseHelper db = new DatabaseHelper(this,null,null,1);
        data = db.getAllOrders();
        adapter = new CustomAdapter(data);
        recyclerView.setAdapter(adapter);
    }
    public void clickBack(View view){
        Intent b = new Intent(viewOrders.this,MainActivity.class);
        startActivity(b);
    }
    public void findOrder(View view){
        DatabaseHelper db = new DatabaseHelper(this,null,null,1);
        DataModel order = db.getOrder(nameBox.getText().toString());

        if (order != null) {
            data.clear();
            data.add(order);
        }
        adapter = new CustomAdapter(data);
        recyclerView.setAdapter(adapter);
    }
}
