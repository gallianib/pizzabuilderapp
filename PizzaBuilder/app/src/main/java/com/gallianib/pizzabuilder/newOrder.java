package com.gallianib.pizzabuilder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;


public class newOrder extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_order);
    }
    public void customizePizza(View view) {
        Intent i = new Intent(newOrder.this,customizePizza.class);
        EditText editText = (EditText) findViewById(R.id.nameField);
        String name = editText.getText().toString();
        i.putExtra("name", name);
        EditText editText2 = (EditText) findViewById(R.id.addressField);
        String address = editText2.getText().toString();
        i.putExtra("address", address);
        EditText editText3 = (EditText) findViewById(R.id.phoneField);
        String phone = editText3.getText().toString();
        i.putExtra("phone", phone);
        EditText editText4 = (EditText) findViewById(R.id.emailField);
        String email = editText4.getText().toString();
        i.putExtra("email", email);
        startActivity(i);
    }

}
