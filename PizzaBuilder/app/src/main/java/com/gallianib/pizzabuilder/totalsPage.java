package com.gallianib.pizzabuilder;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class totalsPage extends AppCompatActivity {

    private StringBuilder toppings;
    private String name;
    private String address;
    private String phone;
    private String email = "email";
    private String total;
    private boolean del;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.totals_page);

        Intent intent = getIntent();
        name = intent.getStringExtra("iname");
        address = intent.getStringExtra("iaddress");
        phone = intent.getStringExtra("iphone");
        email = intent.getStringExtra("iemail");
        toppings = new StringBuilder(intent.getStringExtra("itoppings"));
        total = intent.getStringExtra("itotal");
        del = intent.getBooleanExtra("idel",del);
        TextView n = (TextView)findViewById(R.id.oName);
        TextView a = (TextView)findViewById(R.id.oAddress);
        TextView p = (TextView)findViewById(R.id.oPhone);
        TextView e = (TextView)findViewById(R.id.oEmail);
        TextView top = (TextView)findViewById(R.id.oTopping);
        TextView tot = (TextView)findViewById(R.id.oTotal);
        TextView d = (TextView)findViewById(R.id.oDelivery);
        n.setText(name);
        a.setText(address);
        p.setText(phone);
        e.setText(email);
        top.setText(getOrderToppings(toppings));
        tot.setText("$"+total);
        d.setText(Boolean.toString(del));
    }
    public String getOrderToppings(StringBuilder toppings){
        StringBuilder top = new StringBuilder("");
        for (int r = 0;r<10;r++){
            switch(toppings.charAt(r)){
                case 'y':
                    switch (r){
                        case 0:
                            top.append(", Pepperoni");
                            break;
                        case 1:
                            top.append(", Bacon");
                            break;
                        case 2:
                            top.append(", Ham");
                            break;
                        case 3:
                            top.append(", Sausage");
                            break;
                        case 4:
                            top.append(", Jalapenos");
                            break;
                        case 5:
                            top.append(", Olives");
                            break;
                        case 6:
                            top.append(", Green Peppers");
                            break;
                        case 7:
                            top.append(", Banana Peppers");
                            break;
                        case 8:
                            top.append(", Pineapple");
                            break;
                        case 9:
                            top.append(", Onions");
                            break;
                    }
                    break;
                case 'n':
                    break;
            }
        }
        if (top.charAt(0)==','){
            top.deleteCharAt(0);
        }
        return top.toString();
    }
    public void OrderSubmit(View view){
        DataModel order = new DataModel(name);
        order.setName(name);
        order.setAddress(address);
        order.setPhone(phone);
        order.setEmail(email);
        order.setToppings(toppings.toString());
        order.setTotal(total);
        DatabaseHelper dbc = new DatabaseHelper(this,null,null,1);
        dbc.addOrder(order);
        Intent i = new Intent(this,MainActivity.class);
        startActivity(i);
    }
}
